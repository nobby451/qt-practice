#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    int width = ui->widthSpinBox->text().toInt();
    int height = ui->heightSpinBox->text().toInt();
    int amount = ui->amountSpinBox->text().toInt();
    qDebug() << "width: " << width << "height: " << height << "amount: " << amount;
    QImage* image;
    for (int i = 0; i < amount; i++) {
        image = new QImage(width, height, QImage::Format_ARGB32);
        image->save(QString("dummy%1.png").arg(i), "PNG");
        image->~QImage();
    }
    ui->resultLabel->setText(QString("generate %1 images with width %2 height %3").arg(amount).arg(width).arg(height));
}
